<?php
ini_set('display_errors', 'on');
error_reporting(E_ALL);
require_once '/var/www/html/vendor/autoload.php';

$client = new Predis\Async\Client('tcp://devcache1:6379');

$client->connect(function ($client) {
    echo "Connected to Redis, now listening for incoming messages...\n";

    $client->pubsub('chan-1', function ($event, $pubsub) {
        $message = "Received message `%s` from channel `%s` [type: %s].\n";

        $feedback = sprintf($message,
            $event->payload,
            $event->channel,
            $event->kind
        );

        echo $feedback;

        if ($event->payload === 'quit') {
            $pubsub->quit();
        }
    });
});

$client->getEventLoop()->run();